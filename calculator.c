#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
        int first_operand;
        int second_operand;
        char operation;
        if (argc == 4) {
                first_operand = atoi(argv[1]);
                operation = argv[2][0];
                second_operand = atoi(argv[3]);
                if (operation == '+')
                        printf("%d\n", first_operand + second_operand);
                else if (operation == '-')
                        printf("%d\n", first_operand - second_operand);
                else if (operation == '*')
                        printf("%d\n", first_operand * second_operand);
        }
        return 0;
}
